import webpack from "webpack";
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import {BuildOptions} from "./types/config";

export function buildLoaders(options: BuildOptions): webpack.RuleSetRule[] {

    const cssLoader = {
        test: /\.s[ac]ss$/i,
        use: [
            options.isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
            {
                loader: "css-loader",
                options: {
                    modules: {
                        auto: (path: string) => Boolean(path.includes(".module.")),
                        localIdentName: options.isDev ? "[path][name]__[local]--[hash:base64:5]" : "[hash:base64:8]"
                    }
                }
            },
            "sass-loader",
        ],
    }

    // если не используем тс - нужен babel-loader, чтобы джс перегонять из нового формата в старый
    const typescriptLoader = {
        test: /\.tsx?$/,
        use: 'ts-loader', // loader для обработки файлов из test (ts, tsx)
        exclude: /node_modules/
    }

    return [ // конфигурируем лоадеры, для обработки файлов, которые выходят за рамки js, всякие css, png, ts, giff и т.д.
        typescriptLoader,
        cssLoader
    ]
}