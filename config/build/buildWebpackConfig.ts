import {BuildOptions} from "./types/config";
import webpack from "webpack";
import path from "path";
import {buildPlugins} from "./buildPlugins";
import {buildLoaders} from "./buildLoaders";
import {buildResolvers} from "./buildResolvers";
import {buildDevServer} from "./buildDevServer";

export function buildWebpackConfig(options: BuildOptions): webpack.Configuration {
    const {paths, mode, isDev} = options;
    return {
        mode: mode,
        entry: paths.entry, // стартовая точка приложения
        output: { // сборка приложения, куда
            filename: "[name].[contenthash].js", // добавляем хэш для того, чтобы корректно генерировался файл (решает проблему кэширования)
            path: paths.build,
            clean: true, // подчищаем файлы лишние зи билда, остается финальная сборка
        },
        plugins: buildPlugins(options),
        module: {
            rules: buildLoaders(options)
        },
        resolve: buildResolvers(options),
        devtool: isDev ? 'inline-source-map' : undefined, // sourcemap не добавляются и различные комментарии
        devServer: isDev ? buildDevServer(options) : undefined,
    }
}